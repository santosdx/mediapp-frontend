import { HOST } from './../_shared/var.constant';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Paciente } from '../_model/paciente';
import { Subject } from 'rxjs';

//Esta seccipon @Injectable hace que el servicio sea inyectable en el proyecto.
@Injectable({
  providedIn: 'root'
})
export class PacienteService {

  // Creamos un objeto a ser observado por los observables
  pacienteCambio = new Subject<Paciente[]>();
  mensajeCambio = new Subject<string>();
  url: string = `${HOST}/pacientes`;

  constructor(private http: HttpClient) { }

  /**
   * Método que permite consumir el servicio RestFull de Pacientes 
   * y consultar todos los pacientes de la base de datos.
   */
  listar() {
    // esto retorna un observable
    return this.http.get<Paciente[]>(this.url);
  }
  /**
   * Método que permite consulmir el servicio RestFull del Paciente
   * y consultar un registro de Paciente de la base de datos, pasando
   * como parametro su ID (código o llave).
   */
  listarPacientePorId(id: number) {
    return this.http.get<Paciente>(`${this.url}/${id}`);
  }

  /**
   * Método que permite consulmir el servicio RestFull del Paciente
   * y registrar (insert) un nuevo registro de Paciente en la base 
   * de datos, pasando como parametro el objeto Paciente.
   * @param paciente 
   */
  registrar(paciente: Paciente) {
    return this.http.post(this.url, paciente);
  }

  /**
   * Método que permite consulmir el servicio RestFull del Paciente
   * y modificar (update) un registro de Paciente en la base de
   * datos, pasando como parametro el objeto Paciente.
   * @param paciente 
   */
  modificar(paciente: Paciente) {
    return this.http.put(this.url, paciente);
  }

  /**
   * Método que permite consumir el servicio RestFull del Paciente
   * y eliminar (delete) un registro de Paciente en la base de
   * datos, pasando como parametro el ID (código o llave) del
   * Paciente.
   * @param id 
   */
  eliminar(id: number) {
    return this.http.delete(`${this.url}/${id}`);
  }
}
