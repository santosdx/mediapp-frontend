import { MaterialModule } from './material/material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BuscarComponent } from './pages/buscar/buscar.component';
import { ConsultaComponent } from './pages/consulta/consulta.component';
import { EspecialidadComponent } from './pages/especialidad/especialidad.component';
import { ExamenComponent } from './pages/examen/examen.component';
import { MedicoComponent } from './pages/medico/medico.component';
import { PacienteComponent } from './pages/paciente/paciente.component';
import { ReporteComponent } from './pages/reporte/reporte.component';
import { Not403Component } from './pages/not403/not403.component';
import { HttpClientModule } from '@angular/common/http';
import { EdicionPacienteComponent } from './pages/paciente/edicion-paciente/edicion-paciente.component';
import { IndexComponent } from './pages/index/index.component';
import { EdicionMedicoComponent } from './pages/medico/edicion-medico/edicion-medico.component';
import { EdicionExamenComponent } from './pages/examen/edicion-examen/edicion-examen.component';
import { EdicionEspecialidadComponent } from './pages/especialidad/edicion-especialidad/edicion-especialidad.component';

@NgModule({
  declarations: [
    AppComponent,
    BuscarComponent,
    ConsultaComponent,
    EspecialidadComponent,
    ExamenComponent,
    MedicoComponent,
    PacienteComponent,
    ReporteComponent,
    Not403Component,
    EdicionPacienteComponent,
    IndexComponent,
    EdicionMedicoComponent,
    EdicionExamenComponent,
    EdicionEspecialidadComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
