import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTooltipModule, MatButtonModule, MatTableModule, MatPaginatorModule, MatSortModule, MatIconModule, MatToolbarModule, MatCardModule, MatFormFieldModule, MatInputModule, MatSnackBarModule, MatSidenavModule, MatMenuModule, MatDividerModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [ 
        CommonModule, 
        BrowserAnimationsModule ,
        ReactiveFormsModule, // Permite trabajar con formularios
        MatSidenavModule,
        MatMenuModule,
        MatDividerModule,
        MatTooltipModule,
        MatIconModule,
        MatToolbarModule,
        MatButtonModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatSnackBarModule,
    ],
    exports: [
        ReactiveFormsModule, // Permite trabajar con formularios
        MatSidenavModule,
        MatMenuModule,
        MatDividerModule,
        MatTooltipModule,
        MatToolbarModule,
        MatIconModule,
        MatButtonModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatSnackBarModule,
    ],
    providers: [],
})
export class MaterialModule {}