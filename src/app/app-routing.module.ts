import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './pages/index/index.component';
import { PacienteComponent } from './pages/paciente/paciente.component';
import { MedicoComponent } from './pages/medico/medico.component';
import { ExamenComponent } from './pages/examen/examen.component';
import { EspecialidadComponent } from './pages/especialidad/especialidad.component';
import { ConsultaComponent } from './pages/consulta/consulta.component';
import { BuscarComponent } from './pages/buscar/buscar.component';
import { ReporteComponent } from './pages/reporte/reporte.component';
import { EdicionMedicoComponent } from './pages/medico/edicion-medico/edicion-medico.component';
import { EdicionPacienteComponent } from './pages/paciente/edicion-paciente/edicion-paciente.component';
import { EdicionExamenComponent } from './pages/examen/edicion-examen/edicion-examen.component';
import { EdicionEspecialidadComponent } from './pages/especialidad/edicion-especialidad/edicion-especialidad.component';


const routes: Routes = [
  {path: '', component: IndexComponent},
  {path: 'paciente', component: PacienteComponent, children: [
    {path: 'nuevo', component: EdicionPacienteComponent},
    {path: 'edicion/:id', component: EdicionPacienteComponent}
  ]},
  {path: 'medico', component: MedicoComponent, children: [
    {path: 'nuevo', component: EdicionMedicoComponent},
    {path: 'edicion/:id', component: EdicionMedicoComponent}
  ]},
  {path: 'examen', component: ExamenComponent, children: [
    {path: 'nuevo', component: EdicionExamenComponent},
    {path: 'edicion/:id', component: EdicionExamenComponent}
  ]},
  {path: 'especialidad', component: EspecialidadComponent, children: [
    {path: 'nuevo', component: EdicionEspecialidadComponent},
    {path: 'edicion/:id', component: EdicionEspecialidadComponent}
  ]},
  { path: 'consulta', component: ConsultaComponent },
  //{ path: 'consulta-especial', component: EspecialidadComponent },
  { path: 'buscar', component: BuscarComponent },
  { path: 'reporte', component: ReporteComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
