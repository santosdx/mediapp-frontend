//El ambito (export) permite que la clase sea utilizada en todo el proyecto.
export class   Paciente{

    idPaciente : number;
    nombres : string;
    apellidos : string;
    dni : string;
    direccion : string;
    telefono : string;
    email : string;
}