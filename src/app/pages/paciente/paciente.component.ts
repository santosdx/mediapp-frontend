import { PacienteService } from './../../_service/paciente.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Paciente } from 'src/app/_model/paciente';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html',
  styleUrls: ['./paciente.component.css']
})
export class PacienteComponent implements OnInit {

  dataSourcePaciente: MatTableDataSource<Paciente>;
  displayedColumns = ['idPaciente', 'nombres', 'apellidos', 'acciones'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private pacienteService: PacienteService, private snackBar: MatSnackBar) { }

  //Función que se ejecuta cuando el componente es creado.
  ngOnInit() {

    //Esta sección de código, forma parte de trabajar con la programación reactiva, ya que en archivo 
    //"paciente.service.ts" creamos el objeto observable y en el archivo "edicion-paciente.component.ts"
    //indicamos que el objeto observable cambio (next), por lo tanto, cuando el compoente principal 
    //Paciente se refresque de nuevo, se lanza la función ngOnInit(), y en ella validamos que si cambio
    //obtenemos los datos que le pasaron (que es la lista de paciente) y los agregamos al dataSource
    //De la tabla.
    this.pacienteService.pacienteCambio.subscribe(data => {
      this.dataSourcePaciente = new MatTableDataSource(data);
      this.dataSourcePaciente.paginator = this.paginator;
      this.dataSourcePaciente.sort = this.sort;
      console.log("Se activo el observable Paciente ngOnInit()");
    });
    this.pacienteService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000
      });
    });

    //Con la variable (pacienteService) invocamos el metodo listar() y nos suscribimos para leer los datos.
    this.pacienteService.listar().subscribe(data => {
      this.dataSourcePaciente = new MatTableDataSource(data);
      this.dataSourcePaciente.paginator = this.paginator;
      this.dataSourcePaciente.sort = this.sort;
      console.log("listar() ngOnInit()");
    });
  }

  eliminar(idPaciente: number) {
    this.pacienteService.eliminar(idPaciente).subscribe(data => {
      this.pacienteService.listar().subscribe(data => {
        this.pacienteService.pacienteCambio.next(data);
        this.pacienteService.mensajeCambio.next('Se eliminó el paciente.');
      });
    });
  }

}
