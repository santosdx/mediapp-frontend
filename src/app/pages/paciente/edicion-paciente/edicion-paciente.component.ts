import { PacienteService } from './../../../_service/paciente.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Paciente } from 'src/app/_model/paciente';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-edicion-paciente',
  templateUrl: './edicion-paciente.component.html',
  styleUrls: ['./edicion-paciente.component.css']
})
export class EdicionPacienteComponent implements OnInit {

  id: number;
  formPaciente: FormGroup;
  edicion: boolean = false;
  paciente: Paciente;

  //Inyectamos la instancia del route activo.
  constructor(private routeActivo: ActivatedRoute, private router: Router, private pacienteService: PacienteService) { }

  ngOnInit() {

    //Inicializamos el formulario y le pasamos la estructura inicial.
    this.formPaciente = new FormGroup({
      'id': new FormControl(0),
      'nombres': new FormControl(''),
      'apellidos': new FormControl(''),
      'dni': new FormControl(''),
      'direccion': new FormControl(''),
      'telefono': new FormControl(''),
      'email': new FormControl('')
    });

    this.paciente = new Paciente();
    //Determinamos cuando viene un elemento nuevo y cuando es una edición, esto es para el 
    //flujo del componente de edición, motrar el formulario en blanco o con los datos.

    this.routeActivo.params.subscribe((params: Params) => {
      //Capturamos la parte dinamica de la URL
      this.id = params['id'];
      //Si la url trae id, es un true, es decir editar, si no es nuevo.
      this.edicion = this.id != null;
      this.initFormPaciente();
    });

  }

  /**
   * Cargamos los datos del servicio con la información del Paciente
   * en los campos del formulario.
   */
  initFormPaciente() {
    if (this.edicion) {
      //cargar la data del servicio hacia el formulario
      this.pacienteService.listarPacientePorId(this.id).subscribe(data => {
        this.formPaciente = new FormGroup({
          'id': new FormControl(data.idPaciente),
          'nombres': new FormControl(data.nombres),
          'apellidos': new FormControl(data.apellidos),
          'dni': new FormControl(data.dni),
          'direccion': new FormControl(data.direccion),
          'telefono': new FormControl(data.telefono),
          'email': new FormControl(data.email)
        });
      });
    }
  }

  operar() {
    //Asignamos al objeto nuevo paciente, los valores en sus atributos. los que estan en el formulario
    this.paciente.idPaciente = this.formPaciente.value['id'];
    this.paciente.nombres = this.formPaciente.value['nombres'];
    this.paciente.apellidos = this.formPaciente.value['apellidos'];
    this.paciente.dni = this.formPaciente.value['dni'];
    this.paciente.direccion = this.formPaciente.value['direccion'];
    this.paciente.telefono = this.formPaciente.value['telefono'];
    this.paciente.email = this.formPaciente.value['email'];

    if (this.edicion) {
      //actualizar
      this.pacienteService.modificar(this.paciente).subscribe(data => {
        //Trabajamos programación Reactiva.
        this.pacienteService.listar().subscribe(pacientes => {
          //(next) método para indicar un cambio, y hay que darle la data del cambio (pacientes)
          this.pacienteService.pacienteCambio.next(pacientes);
          this.pacienteService.mensajeCambio.next('Se modificó el paciente.');
        });
      });
    } else {
      //registrar
      this.pacienteService.registrar(this.paciente).subscribe(data => {
        this.pacienteService.listar().subscribe(pacientes => {
          this.pacienteService.pacienteCambio.next(pacientes);
          this.pacienteService.mensajeCambio.next('Se registró el paciente.');
        });
      });
    }

    this.router.navigate(['paciente']);
  }

}
